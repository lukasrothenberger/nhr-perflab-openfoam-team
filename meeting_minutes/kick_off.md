# NHR Perflab: OpenFOAM Kick-Off: 22.09.2023
## Agenda
- Welcome and introduction
- Identification of stakeholders and potential collaborators (discussion)
    - Users
    - Developers
    - Contacts in the OpenFOAM community
        - Members of NHR4CES SDLs?
- Definition of the targeted problem dimensions and connections / differences to ExaFOAM (discussion)
    - E.g. parallelization, optimization, or porting to new architectures
    - Identifications of "available" tools and competencies
        - Identification of possible focus groups?
- Definition of the relevant benchmarks (discussion)
    - Solvers
    - Data sizes
    - Datasets
        - Generation and / or collection
    - Weak vs. strong scaling
    - ...
- Environment, sychronization and communication (discussion)
- First and next steps (discussion)
    - Potentially on a per focus group level?
- Schedule a next meeting   

### Notes
This is only a first draft for the agenda. Please feel free to modify, add, or discard items as you see fit.
If you encounter any problems, please feel free to [contact us via e-mail](mailto:lukas.rothenberger@tu-darmstadt.de).


## Participants
| Affiliation   | Name                  | Attendance    |
|---------------|-----------------------|---------------| 
| TUDa          | Lukas Rothenberger    | ✓             |            
| --            | --                    | --            |


## Minutes
